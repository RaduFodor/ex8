package ro.orange;

import javax.swing.text.html.HTMLDocument;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        // variable definition
        String[] employees = {"Will Smith", "Benjamin McCartney", "Albert Davidson", "Jaden Clinton", "Donald Lincoln",
                "Bart Bureau", "Alistaire O'Doherty"};
        int i, listsize, newsize;


        // method definition
        /*System.out.println(employees.length);
        for (i=0; i < employees.length; i++){
            System.out.println(employees[i]);       // initial verification
        }
        */
        List MyHotelEmployees = new ArrayList();    // instantiate ArrayList class, implementing List interface
        for (String item : employees){
            MyHotelEmployees.add(item);
        }
        listsize = MyHotelEmployees.size();
        System.out.println("The number of employees of the hotel is: " + listsize + "."); // ArrayList size
        System.out.println("Their names are: " + MyHotelEmployees + ".");
        System.out.println("The employee at first floor is: " + MyHotelEmployees.get(1));   // get employee at first
                                                                                            // floor
        MyHotelEmployees.remove(1);         // remove employee at first floor
        System.out.println("The designated employee on the first floor was fired for inappropriate behaviour.");
        System.out.println("The new list of employees is: " + MyHotelEmployees + ".");

        newsize = MyHotelEmployees.size();
        MyHotelEmployees.set(1, "Steve Irwin");     // new employee at first floor
        System.out.println("The new substitute will be " + MyHotelEmployees.get(1));

        System.out.println("The final list of employees is: " + MyHotelEmployees + ".");


    }
}
